// struct for card games

#include <iostream>
#include <conio.h>
#include <string>

using namespace std;

enum Rank // ace high
{
	TWO = 2, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE, TEN, JACK, QUEEN, KING, ACE
};

enum Suit
{
	SPADE, DIAMOND, CLUB, HEART
};

struct Card
{
	Rank rank;
	Suit suit;
};

Card HighCard(Card card1, Card card2) 
{
	if (card1.rank >= card2.rank) {
		return card1;
	}
	else {
		return card2;
	}
};

void PrintCard(Card card)
{
	//Enum to String
	if (card.rank == TWO) cout << "TWO";
	if (card.rank == THREE) cout << "THREE";
	if (card.rank == FOUR) cout << "FOUR";
	if (card.rank == FIVE) cout << "FIVE";
	if (card.rank == SIX) cout << "SIX";
	if (card.rank == SEVEN) cout << "SEVEN";
	if (card.rank == EIGHT) cout << "EIGHT";
	if (card.rank == NINE) cout << "NINE";
	if (card.rank == TEN) cout << "TEN";
	if (card.rank == JACK) cout << "JACK";
	if (card.rank == QUEEN) cout << "QUEEN";
	if (card.rank == KING) cout << "KING";
	if (card.rank == ACE) cout << "ACE";

	cout << " OF ";

	if (card.suit == CLUB) cout << "CLUBS";
	if (card.suit == HEART) cout << "HEARTS";
	if (card.suit == SPADE) cout << "SPADES";
	if (card.suit == DIAMOND) cout << "DIAMONDS";

}

int main()
{
	//Declare struct and variables
	string confirm;

	Card card1;
	card1.rank = Rank(rand() % ACE);
	card1.suit = Suit(rand() % HEART);

	Card card2;
	card2.rank = Rank(rand() % ACE);
	card2.suit = Suit(rand() % HEART);

	//Output
	PrintCard(card1);
	cout << "\n";
	PrintCard(card2);
	cout << "\n\n";
	cout << "Winner is: ";
	PrintCard(HighCard(card1,card2));

	cout << "\n\n";

	//Challenge them
	cout << "Duel again? (y/n)\n";
	cin >> confirm;
	cout << "\n\n";
	if (confirm.compare("n") != 0) {
		//Recursion
		main();
	}
	else {
		//Shame
		cout << "Coward!";
	}


	_getch();
	return 0;
}
